﻿
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include "sqlite3.h"
#include "Input_data.cpp"

Input_data input_data;
//SQL_reis sql;//создает экземпляр класса

//функция нужна для вывода данных из БД
int CallBack(void* data, int argc, char** argv, char** ColName)
{
	for (int i = 0; i < argc; i++)
		std::cout << argv[i]<<" | ";
	std::cout << " " << std::endl;
	return 0;
}

//класс для создания SQL запроса с параметрами пользователя (запрос нужен для вывода данных из БД SQLITE)
class SQL_reis
{
public:
	char SQL[1000] = "SELECT * FROM reis where [Departure city]='";
	void sql_reis(){
	strcat(SQL, input_data.dс);
	strcat(SQL, "' and [City of arrive]='");
	strcat(SQL, input_data.сa);
	strcat(SQL, "' and Baggage ='");
	strcat(SQL, input_data.b);
	strcat(SQL, "' and Date ='");
	strcat(SQL, input_data.d);
	strcat(SQL, "' order by Price "); }
};

int main()
{
	setlocale(LC_ALL, "Russian");
	SQL_reis sql;//создает экземпляр класса
	input_data.data_reis(); //вызываем метод класса
	
	fprintf(stderr, "Город отправ|Гор приб|Авиакомпан|Дата отправл| Время |Баг  | Цена |\n\n");
	sqlite3* db=0;//переменная для работы с базой данных
	char* error = 0;
	int rc;

	rc = sqlite3_open("reis1.db", &db);

	if (rc) {
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		return(0);
	}
	else {
		//fprintf(stderr, "Opened database successfully\n");
	}
	sql.sql_reis();//вызывает метод из класса SQL_reis

	rc = sqlite3_exec(db, sql.SQL, &CallBack, (void*)10, &error);//выполняет созданный запрос в БД
	if (rc != SQLITE_OK)
	{
		std::cout << error << std::endl;
		sqlite3_free(error);
	}
	sqlite3_close(db);
	return 0;
}

